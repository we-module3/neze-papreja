# We Module 3 Gen AI
## Neze Papreja
Artificial intelligence technology known as "generative AI" is capable of producing text, images, audio, and synthetic data, among other kinds of content. The ease of use of new user interfaces that enable the creation of excellent text, pictures, and movies in a matter of seconds has been the driving force behind the recent excitement surrounding generative AI.

It should be mentioned that the technology is not entirely new. Chatbots were the first applications of generative AI in the 1960s. But it wasn't until 2014 that generative AI was able to produce believable realistic images, videos, and audio of actual people thanks to the development of generative adversarial networks, or GANs, a kind of machine learning algorithm.
# This repo is to keep a tab of the work done for GenAI sessions
'GenAI-transcripts.csv' contains all the transcripts of my conversations with the GenAI to achieve the results. 
'GenAI-colab-links.csv' contains the final code results.
## Work done

- Solved computational thinking problem: TSP using GenAI
- Understood the game of Yahtzee, engineered the genAI tool to play Yahtzee, asked it on how to approach the Yahtzee scorer code.
- Formed testing strategy for the Yahtzee scorer code
- Wrote code for Markov Chains