// NoSQL

db.createCollection("Pokemon");

db.createCollection("Type");

db.createCollection("Move");

db.createCollection("Type_Effectiveness");

// Insert data into Pokemon collection
db.Pokemon.insertMany([
  { _id: 1, pokemon_name: "Bulbasaur", primary_type: "Grass" },
  { _id: 2, pokemon_name: "Charmander", primary_type: "Fire" },
  { _id: 3, pokemon_name: "Squirtle", primary_type: "Water" },
  { _id: 4, pokemon_name: "Eevee", primary_type: "Normal" },
  { _id: 5, pokemon_name: "Pidgey", primary_type: "Normal", secondary_type: "Flying" }
]);

// Insert data into Type collection
db.Type.insertMany([
  { _id: 1, type_name: "Normal" },
  { _id: 2, type_name: "Fire" },
  { _id: 3, type_name: "Water" },
  { _id: 4, type_name: "Grass" },
  { _id: 5, type_name: "Flying" }
]);

// Insert data into Move collection
db.Move.insertMany([
  { _id: 1, move_name: "Tackle", power: 35, type_id: 1 },
  { _id: 2, move_name: "Vine Whip", power: 40, type_id: 4 },
  { _id: 3, move_name: "Return", power: 100, type_id: 1 },
  { _id: 4, move_name: "Ember", power: 40, type_id: 2 },
  { _id: 5, move_name: "Water Gun", power: 40, type_id: 3 },
  { _id: 6, move_name: "Wing Attack", power: 65, type_id: 5 },
  { _id: 7, move_name: "Headbutt", power: 70, type_id: 1 }
]);

// Insert data into Type_Effectiveness collection
db.Type_Effectiveness.insertMany([
  { type_id: 2, effective_against_type_id: 4, ineffective_against_type_id: 3 },
  { type_id: 3, effective_against_type_id: 2, ineffective_against_type_id: 4 },
  { type_id: 4, effective_against_type_id: 3, ineffective_against_type_id: 2 },
  { type_id: 4, effective_against_type_id: 5 },
  { type_id: 5, effective_against_type_id: 4 },
  { type_id: 1 }
]);


db.Pokemon.find({ "pokemon_name": { $in: db.Pokemon_Move.find({ "move_id": db.Move.findOne({ "move_name": "Return" })._id }).toArray().map(m => m.pokemon_name) } }, { "pokemon_name": 1, "_id": 0 })

db.Move.find({ "type_id": { $in: db.Type_Effectiveness.find({ "effective_against_type_id": db.Type.findOne({ "type_name": "Grass" })._id }).toArray().map(t => t.type_id) } })

