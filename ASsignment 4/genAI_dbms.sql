-- Create Pokemon table
create database genAI;
use genAI;
CREATE TABLE Pokemon (
    pokemon_id INT PRIMARY KEY,
    pokemon_name VARCHAR(50),
    primary_type VARCHAR(20),
    secondary_type VARCHAR(20)
);

-- Create Type table
CREATE TABLE Type (
    type_id INT PRIMARY KEY,
    type_name VARCHAR(20)
);

-- Create Move table
CREATE TABLE Move (
    move_id INT PRIMARY KEY,
    move_name VARCHAR(50),
    power INT,
    type_id INT,
    FOREIGN KEY (type_id) REFERENCES Type(type_id)
);

-- Create Pokemon_Move table for many-to-many relationship
CREATE TABLE Pokemon_Move (
    pokemon_id INT,
    move_id INT,
    FOREIGN KEY (pokemon_id) REFERENCES Pokemon(pokemon_id),
    FOREIGN KEY (move_id) REFERENCES Move(move_id),
    PRIMARY KEY (pokemon_id, move_id)
);

-- Create Type_Effectiveness table
CREATE TABLE Type_Effectiveness1 (
    type_id INT,
    effective_against_type_id INT,
    ineffective_against_type_id INT,
    FOREIGN KEY (type_id) REFERENCES Type(type_id),
    FOREIGN KEY (effective_against_type_id) REFERENCES Type(type_id),
    FOREIGN KEY (ineffective_against_type_id) REFERENCES Type(type_id)
);




-- Insert values into Pokemon table
INSERT INTO Pokemon (pokemon_id, pokemon_name, primary_type, secondary_type)
VALUES 
(1, 'Bulbasaur', 'Grass', NULL),
(2, 'Charmander', 'Fire', NULL),
(3, 'Squirtle', 'Water', NULL),
(4, 'Eevee', 'Normal', NULL),
(5, 'Pidgey', 'Normal', 'Flying');

-- Insert values into Type table
INSERT INTO Type (type_id, type_name)
VALUES 
(1, 'Normal'),
(2, 'Fire'),
(3, 'Water'),
(4, 'Grass'),
(5, 'Flying');

-- Insert values into Move table
INSERT INTO Move (move_id, move_name, power, type_id)
VALUES 
(1, 'Tackle', 35, 1),
(2, 'Vine Whip', 40, 4),
(3, 'Return', 100, 1),
(4, 'Ember', 40, 2),
(5, 'Water Gun', 40, 3),
(6, 'Wing Attack', 65, 5),
(7, 'Headbutt', 70, 1);

-- Insert values into Pokemon_Move table
INSERT INTO Pokemon_Move (pokemon_id, move_id)
VALUES 
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 4),
(2, 3),
(3, 1),
(3, 5),
(3, 3),
(4, 1),
(4, 7),
(4, 3),
(5, 1),
(5, 6),
(5, 3);

INSERT INTO Type_Effectiveness1 (type_id, effective_against_type_id, ineffective_against_type_id)
VALUES 
(2, 4, 3),
(3, 2, 4),
(4, 3, 2),
(4, NULL, 5),
(5, 4, NULL),
(1, NULL, NULL);



SELECT p.pokemon_name
FROM Pokemon p
JOIN Pokemon_Move pm ON p.pokemon_id = pm.pokemon_id
JOIN Move m ON pm.move_id = m.move_id
WHERE m.move_name = 'Return';

SELECT m.move_name
FROM Move m
JOIN Type t ON m.type_id = t.type_id
JOIN Type_Effectiveness1 te ON t.type_id = te.effective_against_type_id
JOIN Type grass ON grass.type_name = 'Grass'
WHERE te.effective_against_type_id = grass.type_id;

